package com.example.firebasetemplate;

import static android.content.ContentValues.TAG;

import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.firebasetemplate.databinding.FragmentRegisterBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.storage.FirebaseStorage;

import java.net.URI;
import java.util.UUID;


public class RegisterFragment extends AppFragment {
    private FragmentRegisterBinding binding;
    private Uri uriImagen;
    private String downloadUriImagen;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return (binding = FragmentRegisterBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.previaUserImage.setOnClickListener(v -> galeria.launch("image/*"));

        appViewModel.uriImagenSeleccionada.observe(getViewLifecycleOwner(), uri -> {
            Glide.with(this).load(uri).into(binding.previaUserImage);
            uriImagen = uri;
        });

        binding.createAccountButton.setOnClickListener(v -> {
            if (binding.nameEditText.getText().toString().isEmpty()) {
                binding.nameEditText.setError("Required name.");
                return;
            }
            if (binding.emailEditText.getText().toString().isEmpty()) {
                binding.emailEditText.setError("Required email.");
                return;
            }
            if (binding.passwordEditText.getText().toString().isEmpty()) {
                binding.passwordEditText.setError("Required password.");
                return;
            }
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(
                    binding.emailEditText.getText().toString(),
                    binding.passwordEditText.getText().toString()
            ).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    FirebaseStorage.getInstance()
                            .getReference("/images/"+ UUID.randomUUID()+".jpg")
                            .putFile(uriImagen)
                            .continueWithTask(task1 -> task1.getResult().getStorage().getDownloadUrl())
                            .addOnSuccessListener(urlDescarga -> {
                                downloadUriImagen = urlDescarga.toString();

                                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                                if (user != null) {
                                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                            .setDisplayName(binding.nameEditText.getText().toString())
                                            .setPhotoUri(Uri.parse(downloadUriImagen))
                                            .build();
                                    user.updateProfile(profileUpdates)
                                            .addOnCompleteListener(task12 -> {
                                                if (task12.isSuccessful()) {
                                                    Log.d(TAG, "User profile updated.");
                                                }
                                            });
                                }

                                navController.navigate(R.id.action_registerFragment_to_postsHomeFragment);
                            });
                } else {
                    Log.d("FAIL", "createUserWithEmail:failure", task.getException());
                    Toast.makeText(requireContext(), task.getException().getLocalizedMessage(),
                            Toast.LENGTH_LONG).show();
                }
            });
        });
    }

    private final ActivityResultLauncher<String> galeria = registerForActivityResult(new ActivityResultContracts.GetContent(), uri -> {
        appViewModel.setUriImagenSeleccionada(uri);
    });
}