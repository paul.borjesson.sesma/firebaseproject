package com.example.firebasetemplate.model;

import com.google.firebase.firestore.Exclude;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Post {
    @Exclude
    public String postid;
    public String content;
    public String authorName;
    public String date;
    public String imageUrl;
    public String userId;

    public HashMap<String, Boolean> likes = new HashMap<>();
    public List<Comment> comments = new ArrayList<>();

}
