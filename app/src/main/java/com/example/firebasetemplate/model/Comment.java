package com.example.firebasetemplate.model;

public class Comment {
    public String contenido;
    public String authorName;
    public String date;
    public String userId;

    public Comment() {
    }

    public Comment(String userId, String authorName, String date, String contenido) {
        this.userId = userId;
        this.authorName = authorName;
        this.date = date;
        this.contenido = contenido;
    }
}
