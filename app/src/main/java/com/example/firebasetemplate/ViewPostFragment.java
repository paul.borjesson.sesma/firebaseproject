package com.example.firebasetemplate;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.firebasetemplate.databinding.FragmentViewPostBinding;
import com.example.firebasetemplate.model.Comment;
import com.example.firebasetemplate.model.Comments;
import com.example.firebasetemplate.model.Post;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;


public class ViewPostFragment extends AppFragment {


    private FragmentViewPostBinding binding;
    private CommentAdapter commentAdapter;

    public ViewPostFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return (binding = FragmentViewPostBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // consultar en la coleccion posts ese postid, y mostrarlo en pantalla
        String postid = ViewPostFragmentArgs.fromBundle(getArguments()).getPostid();

        binding.postid.setText(postid);
        // puedes añadir debajo un recycler para mostrar los comentarios de ese post

        commentAdapter = new CommentAdapter(new ArrayList<Comment>(), requireContext() );
        binding.comentariosRView.setAdapter(commentAdapter);
        binding.comentariosRView.setLayoutManager(new LinearLayoutManager(getContext()));

        // y un text y un boton para añadir un comentario

        binding.buttonComment.setOnClickListener(v -> {
            if (binding.commentBox.getText().toString() != null && binding.commentBox.getText().toString() != "") {
                db.collection("posts").document(postid).collection("comments").document("ins")
                //db.collection("posts").document(postid)
                        .update("comments", FieldValue.arrayUnion(new Comment(auth.getUid(), auth.getCurrentUser().getDisplayName(), LocalDateTime.now().toString(), binding.commentBox.getText().toString())));
            }
        });

        //EL problema de esto es que descarga todos los comentarios cuando recoge un post :/
        // consultar los datos del posts    db.collection("posts").document(postid)
        /*db.collection("posts").document(postid).addSnapshotListener((value, error) -> {
            Post post = value.toObject(Post.class);
            commentAdapter.updateMap(post.comments);
        });*/

        db.collection("posts").document(postid).collection("comments").document("ins").addSnapshotListener((value, error) -> {
            // este codigo se ejecutara cada vez que cambie algo del post en la base de datos
            if (value != null) {
                Comments comments = value.toObject(Comments.class);
                commentAdapter.updateMap(comments);
            }
        });

    }

    class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {

        private List<Comment> listComments;
        private Context context;

        public CommentAdapter(List<Comment> listComments, Context context) {
            this.listComments = listComments;
            this.context = context;
        }

        @NonNull
        @Override
        public CommentAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View view = inflater.inflate(R.layout.my_comment, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull CommentAdapter.ViewHolder holder, int position) {
            holder.useridfield.setText(listComments.get(position).userId);
            holder.usernamefield.setText(listComments.get(position).authorName);
            holder.commentfield.setText(listComments.get(position).contenido);
            holder.datefield.setText(listComments.get(position).date);
        }


        @Override
        public int getItemCount() {
            return listComments.size();
        }

        public void updateMap(List<Comment> listComments) {
            this.listComments = listComments;
            notifyDataSetChanged();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{
            TextView useridfield;
            TextView usernamefield;
            TextView commentfield;
            TextView datefield;
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                useridfield = itemView.findViewById(R.id.userId);
                usernamefield = itemView.findViewById(R.id.userName);
                commentfield = itemView.findViewById(R.id.comment);
                datefield = itemView.findViewById(R.id.date);
            }
        }
    }
}